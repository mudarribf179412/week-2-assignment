package com.example.smd;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText email;
    EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email=findViewById(R.id.editTextTextEmailAddress);
        password=findViewById(R.id.editTextTextPassword);
    }

    public void login(View view) {
        Toast.makeText(view.getContext(),"Email:"+email.getText().toString()+" \nPassword:"+password.getText().toString(),Toast.LENGTH_LONG).show();
    }
}